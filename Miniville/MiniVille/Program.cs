﻿using System;

namespace MiniVille
{
    enum Effect
    {
        Steal,
        Gain
    }
    enum MonumentEffect
    {
        DoubleDices_ThenPlayAgain,
        RerollDices_Once,
        ThrowTwoDices,
        SpeEstablishmentEarnMore
    }
    enum Turn
    {
        PlayerTurn,
        EnemyTurn
    }
    enum CardType
    {
        Champs, // Épis de blé
        Elevage, // Vache
        Ressources, // Engrenage
        Usine, // Usine
        Commerce, // Tranche de pain de mie
        Local, // Bourse
        Restauration, // Tasse
        Monument
    }
    

    class Program
    {


        static void Main(string[] args)
        {
            Game game = new Game();

            game.OnRun();
        }
    }
}
