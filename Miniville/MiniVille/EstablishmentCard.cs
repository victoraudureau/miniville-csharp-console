﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MiniVille
{
    class EstablishmentCard : Card
    {
        public Effect effect;
        public Turn turnActivation;
        public string colour;
        public int[] activationCost;
        public CardType cardType;
        public EstablishmentCard(int[] activationCost, string colour, string name, int cost, CardType cardType) : base(name, cost)
        {
            this.colour = colour;
            this.activationCost = activationCost;
            this.cardType = cardType;

            //switch (colour)
            //{
            //    case "Bleu":
            //        this.turnActivation = Turn.AnyTurn;
            //        effect = Effect.Gain;
            //        break;
            //    case "Vert":
            //        this.turnActivation = Turn.SelfTurn;
            //        effect = Effect.Gain;
            //        break;
            //    case "Rouge":
            //        this.turnActivation = Turn.EnemyTurn;
            //        effect = Effect.Steal;
            //        break;
            //    case "Violet":
            //        this.turnActivation = Turn.AnyTurn;
            //        effect = Effect.Steal;
            //        break;
            //    default:
            //        break;
            //}
        }
    }
}
