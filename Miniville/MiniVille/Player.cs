﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace MiniVille
{
    class Player
    {
        public List<EstablishmentCard> establishmentCards = new List<EstablishmentCard>();
        public List<MonumentCard> monumentCards = new List<MonumentCard>();
        public List<Dice> dices = new List<Dice>();
        public bool isIA;

        public int money;

        public Player(bool isIA = false)
        {
            this.isIA = isIA;

            dices.Add(new Dice(6));
            
            monumentCards.Add(new MonumentCard("Gare", 4));
            monumentCards.Add(new MonumentCard("Centre commercial", 10));
            monumentCards.Add(new MonumentCard("Parc d'attractions", 16));
            monumentCards.Add(new MonumentCard("Tour radio", 22));

            money = 3;
        }
        
        public void BuyEstablishmentCard(EstablishmentCard boughtCard)
        {
            money -= boughtCard.cost;
            establishmentCards.Add(boughtCard);
        }

        public void BuyMonumentCard(MonumentCard boughtCard)
        {
            money -= boughtCard.cost;
            foreach (MonumentCard card in monumentCards)
            {
                if (card.name == boughtCard.name) card.isBuilt = true;
            }
        }

        public void AddEstablishmentCard(EstablishmentCard card) { 
            establishmentCards.Add(card);
        }

        public bool hasEveryMonuments()
        {
            int count = 0;
            foreach(MonumentCard monument in monumentCards)
            {
                if (monument.isBuilt)
                    count++;
            }
            return count == 4;
        }

        public void PlayerTurn()
        {
           

            //if (monumentCards[1].isBuilt)
            //{
            //    Console.WriteLine("Souhaitez vous relancer vos dés ?\nO - Oui\nN - Non");
            //    string choix = Console.ReadLine();

            //    choix = choix.ToUpperInvariant();
            //    if (choix == "O")
            //    {
            //        foreach (Dice d in dices)
            //        {
            //            d.Throw();
            //        }
            //    }
            //    Console.Clear();
            //}


            //if ( dices[0].face == dices[1].face && monumentCards[0].isBuilt)
            //{
            //    Console.WriteLine("Vous avez le droit de jouer à nouveau");
            //    Thread.Sleep(2000);
            //    Console.Clear();
            //    PlayerTurn();
            //}
        }

        public bool FindCard(string cardName)
        {
            foreach (EstablishmentCard card in establishmentCards)
            {
                if (card.GetName() == cardName)
                {
                    return true;
                }
            }
            foreach (MonumentCard card in monumentCards)
            {
                if (card.GetName() == cardName)
                {
                    return true;
                }
            }
            return false;
        }

        public int ThrowDices()
        {
            int result = 0;
            foreach(Dice dice in dices)
            {
                dice.Throw();
                result += dice.face;
            }
            return result;
        }

        

    }
}
